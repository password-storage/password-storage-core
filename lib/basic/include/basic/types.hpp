#pragma once

#include <vector>

namespace ps {
    using bytes = std::vector<uint8_t>;
}// namespace ps