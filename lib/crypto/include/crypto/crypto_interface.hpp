#pragma once

#include <basic/types.hpp>

namespace ps {

class CryptoMessageInterface {
public:
    virtual bytes encrypt(const bytes&) const = 0;
    virtual bytes decrypt(const bytes&) const = 0;
};

} // namespace ps