#pragma once

#include <crypto/crypto_interface.hpp>

namespace ps {

class AESCryptoMessage: public CryptoMessageInterface {
public:
    virtual bytes encrypt(const bytes&) const override;
    virtual bytes decrypt(const bytes&) const override;
};

} // namespace ps