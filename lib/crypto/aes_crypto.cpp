#include <crypto/aes_crypto.hpp>

namespace ps {

bytes AESCryptoMessage::encrypt(const bytes& plain_text) const {
    return plain_text;
}
bytes AESCryptoMessage::decrypt(const bytes& cipher_text) const {
    return cipher_text;
}

} // namespace ps