cmake_minimum_required(VERSION 3.18 FATAL_ERROR)
project(password_storage)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS ON)

if(NOT EXISTS "${PROJECT_BINARY_DIR}/conan.cmake")
   message(STATUS "Downloading conan.cmake from https://github.com/conan-io/cmake-conan")
   file(DOWNLOAD "https://github.com/conan-io/cmake-conan/raw/v0.15/conan.cmake"
                 "${PROJECT_BINARY_DIR}/conan.cmake")
endif()
include(${PROJECT_BINARY_DIR}/conan.cmake)

conan_cmake_run(REQUIRES boost/1.71.0@conan/stable
                OPTIONS
                boost:without_atomic=True
                boost:without_chrono=True
                boost:without_container=True
                boost:without_context=True
                boost:without_contract=True
                boost:without_coroutine=True
                boost:without_date_time=True
                boost:without_exception=True
                boost:without_fiber=True
                boost:without_filesystem=True
                boost:without_graph=True
                boost:without_graph_parallel=True
                boost:without_iostreams=True
                boost:without_locale=True
                boost:without_log=True
                boost:without_math=True
                boost:without_mpi=True
                boost:without_program_options=True
                boost:without_python=True
                boost:without_random=True
                boost:without_regex=True
                boost:without_serialization=True
                boost:without_stacktrace=True
                boost:without_system=True
                boost:without_test=False
                boost:without_thread=True
                boost:without_timer=True
                boost:without_type_erasure=True
                boost:without_wave=True
                BUILD missing
)

conan_basic_setup(TARGETS)
find_package(Boost 1.71.0 EXACT REQUIRED COMPONENTS unit_test_framework)

add_subdirectory(lib)
add_subdirectory(tests)
