#define BOOST_TEST_MODULE Basic crypto tests
#include <boost/test/unit_test.hpp>
#include <crypto/aes_crypto.hpp>

BOOST_AUTO_TEST_SUITE(basic_crypto_tests)

BOOST_AUTO_TEST_CASE(test1) {
    ps::AESCryptoMessage aes;
    auto enc = ps::bytes{1, 2, 3, 4};

    BOOST_CHECK_EQUAL(aes.encrypt(enc).size(), enc.size());
}

BOOST_AUTO_TEST_SUITE_END()